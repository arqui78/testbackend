TestBackend

Proyecto implementado en NodeJs,MongoDB
Sailsjs como frameword.


Dependencias
* npm install sails-mongo --save

Despliegue
npm install
sails lift //Desarrollo
sails lift --prod //Produccion

El Backdend esta constituido por un modelo (Users) y su controlador el cual atiende las peticiones http, y realiza las transacciones a la base de dato, la cual esta diseñada en MongoDB.
API RestFul

Lista de Usuarios
GET http://localhost:1337/Users 

Crear Usuario 
Única validación: que no sea undefined
POST http://localhost:1337/User 

Actualizar Usuario 
PUT http://localhost:1337/User/id

Eliminar Usuario 
DELETE http://localhost:1337/User/id

////////////////////////////////////
Los Atributos del JSON son sensible a la minúscula
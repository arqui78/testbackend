/**
 * Users.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    status : {
      type : 'string',
    },
    name : {
      type : 'string',
    },
    gender : {
      type : 'string',
    },
    company : {
      type : 'string',
    },
    email : {
      type : 'string',
    },
    phone : {
      type : 'string',
    },
    address : {
      type : 'string',
    }
  }
};


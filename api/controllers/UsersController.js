/**
 * UsersController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  create : function(req,res,next){
    if(!req.isSocket){
      console.log("Datos Recibidos");
      try{
        var data = {
          front : req.params.all(),
          back : {},
          session : {}
        };

        /**
         * Validando que los datos no sean undefined
         * Se pueden implementar otra validacion en
         * un instancia que permita validar otra reglas
         */
        if (_.isEqual(undefined,data.front.name) || _.isEqual(undefined,data.front.gender) || _.isEqual(undefined,data.front.company) ||
           _.isEqual(undefined,data.front.email) || _.isEqual(undefined,data.front.phone) || _.isEqual(undefined,data.front.address)){
            return res.json({error:true, content:'Los datos son requeridos - ¡Solo se esta validando undefined! - Atributos en minusculas'});
        };

        create(data);
      }catch(error){
        console.log("UsersController > create > 0: "+error);
        res.json({error:true,message: 'Lo sentimos, hemos tenido un problema en el servidor","Por favor intentalo más tarde',content:' '});
      }

      function create(data){
        try{
          var dataToCreate = {
              status: 'Active',
              name: data.front.name,
              gender: data.front.gender,
              company: data.front.company,
              email: data.front.email,
              phone: data.front.phone,
              address: data.front.address
          };

          Users.create(dataToCreate).exec(function(error,user){
            if(!error && user){
                res.json(201,{error:false,message :'Registro Exitoso',content : user});
            }else{
              console.log("Error  Users.create() : "+error);
              res.json({error:true,message:'Failed',content:' '});
            }
          });

        }catch(error){
          console.log("UsersController > create > 1: "+error);
          res.json({error:true,message: 'Lo sentimos, hemos tenido un problema en el servidor","Por favor intentalo más tarde',content:' '});
        }
      }
    }
  },
  getUsers : function(req,res,next){
    if(!req.isSocket){
      try{
        var data = {
          front : req.params.all(),
          back : {},
          session : {}
        };

        Users.find().exec(function(error,users){
          if(!error){
            if(users.length > 0) {
              res.json({error:false,message: 'Success ',content:users});
            }else{
              res.json({error:true,message:'No hay registros',content:' '});
            }
          }else{
            console.log("Error Users.find : "+error);
            res.json({error:true,message:'Failed',content:' '});
          }
        });
      }catch(error){
        console.log("UsersController > gets > 0: "+error);
        res.json({error:true,message: 'Lo sentimos, hemos tenido un problema en el servidor","Por favor intentalo más tarde',content:' '});
      }
    }
  },
  getUserById : function(req,res,next){
    if(!req.isSocket){
      try{
        var data = {
          front : req.params.all(),
          back : {},
          session : {}
        };

        Users.find(data.front.id).exec(function(error,user){
          if(!error){
            if(user.length > 0) {
              res.json({error:false,message: 'Success ',content:user[0]});
            }else{
              res.json({error:true,message:'No exite el usuario',content:' '});
            }
          }else{
            console.log("Error Users.find : "+error);
            res.json({error:true,message:'Failed',content:' '});
          }
        });
      }catch(error){
        console.log("UsersController > etUserById > 0: "+error);
        res.json({error:true,message: 'Lo sentimos, hemos tenido un problema en el servidor","Por favor intentalo más tarde',content:' '});
      }
    }
  },
   updateUser : function(req,res,next){
    if(!req.isSocket){
      try{
        var data = {
          front : req.params.all(),
          back : {},
          session : {}
        };

        var update = {
            status: data.front.status,
            name: data.front.name,
            gender: data.front.gender,
            company: data.front.company,
            email: data.front.email,
            phone: data.front.phone,
            address: data.front.address
          };

        /*
         * Antes de Actualizar es necesario implementar Validación de los Datos
         */

        Users.update(data.front.id,update).exec(function(error,user){
          if(!error){
            if(user.length > 0) {
              res.json({error:false,message: 'Usuario Actualizado ',content:user[0]});
            }else{
              res.json({error:true,message:'No exite el usuario',content:' '});
            }
          }else{
            console.log("Error Users.update() : "+error);
            res.json({error:true,message:'Failed',content:' '});
          }
        });
      }catch(error){
        console.log("UsersController > updateUser > 0: "+error);
        res.json({error:true,message: 'Lo sentimos, hemos tenido un problema en el servidor","Por favor intentalo más tarde',content:' '});
      }
    }
  },
  deleteUser : function(req,res,next){
    if(!req.isSocket){
      try{
        var data = {
          front : req.params.all(),
          back : {},
          session : {}
        };

        Users.destroy(data.front.id).exec(function(error){
          if(!error){
            res.json({error:false,message: 'Usuario Eliminado con Exito ',content:''});
          }else{
            console.log("Error  Users.delete() : "+error);
            res.json({error:true,message:'No exite el usuario',content:' '});
          }
        });
      }catch(error){
        console.log("UsersController > deleteUser > 0: "+error);
        res.json({error:true,message: 'Lo sentimos, hemos tenido un problema en el servidor","Por favor intentalo más tarde',content:' '});
      }
    }
  },
};

